El Plugin descarga un PDF con un formato más simple, el listado de todos los productos de la tienda ordenado por Categorías y que incluya las imágenes de dichos productos.

El plugin es sólo es de acceso del administrador, y únicamente se debe acceder al listado de productos de WooCommerce donde aparece un botón para descargar el PDF.

![alt text](https://diurvanconsultores.com/wp-content/uploads/2020/03/woo-export-products-pdf2-1024x449.jpg?raw=true)
